#!/usr/bin/env python
#coding: utf-8

"""
Some Things we want to do in post-processing:

OUTPUTS
 X constraint plots 
 X constraint stats
 X best-fit stats
 - convergence tests
 - cosmology data for one cosmology 
 - latex parameter table
 - auto-determine burn-in?
 
RUNNING
 - run on either chain or ini file
 
"""

import argparse

def plotting(args):
	from cosmosis.plotting import CosmologyPlotter
	filetype=args.format
	plotter = CosmologyPlotter.from_chain_files(
		args.filenames,
		burn=args.burn,
		thin=args.thin,
		latex_file=args.latex,
		filetype=filetype,
		fill=not args.nofill,
		root_dir=args.root_dir,
		prefix=args.prefix,
		)
	plotter.plot_1d_params(args.params)
	if not args.one_d:
		plotter.plot_2d_params(args.params)
	if args.w0_wa:
		plotter.w0_wa_plot()
	return plotter

def statistics(args):
	from cosmosis.runtime.analytics import Analytics
	analytics = Analytics.from_chain_files(args.filenames, burn=args.burn, thin=args.thin)
	Mu =  analytics.trace_means()
	Sigma = analytics.trace_variances()**0.5
	print
	print "Marginalized:"
	for p, mu, sigma in zip(analytics.params, Mu, Sigma):
		if p=="LIKE":continue
		print '    %s = %g ± %g' % (p, mu, sigma)
	print
	if analytics.best_index==None:
		print "Could not see LIKE column to get best likelihood"
	else:
		print "Best likelihood:"
		print "    Index = %d" % (analytics.best_index)
		for p, v in zip(analytics.params, analytics.best_params):
			print '    %s = %g' % (p, v)
	print

description = "Plot marginalized constraints from a des-pipe MCMC output chain."
parser = argparse.ArgumentParser(description=description, add_help=True)

#What-to-do arguments
parser.add_argument('--stats', action='store_true',
	help='No plotting, only stats')

#Chain data arguments
parser.add_argument('filenames', metavar='filenames', type=str, nargs='+',
	help='Chain filenames')
parser.add_argument("-t","--thin", type=int, default=1, 
	help='Thinning to apply to chains (after burn)')
parser.add_argument("-b","--burn", type=float, default=0,
	help='Fraction or number of samples to cut at the start of the chain (before thinning)')
parser.add_argument("-l","--latex", type=str, default="",
	help='Ini file containing latex names for parameters')
parser.add_argument('-o', '--root-dir', default='.', 
	help='The root directory in which to put plots')
parser.add_argument('-p', '--prefix', default='', 
	help='A prefix to give to all the generated filenames')


# Plotting arguments
parser.add_argument('-P', '--param', dest='params', action='append',
	help='List params to make plots of (e.g. COSMOPAR--H0).  Default is to do all.')
parser.add_argument('-1', '--one-d', dest='one_d', action='store_true',
	help='Make only 1D plots, not contours')
parser.add_argument('-w', '--w0-wa', dest='w0_wa', action='store_true',
	help='Make the special W0-Wa plot')
parser.add_argument('-f', '--format', dest='format', default='png', type=str,
	help='Output image filename suffix; determines type. e.g. png, pdf, eps')
parser.add_argument('-L', '--no-fill', dest='nofill', action='store_true',
	help='Produce line contour plots instead of filled')


if __name__=="__main__":
	args = parser.parse_args()
	if not args.stats:
		plotting(args)
	statistics(args)
